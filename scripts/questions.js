//build.js
const inquirer = require('inquirer');
// const copydir = require('copy-dir');
// const mkdirp = require('mkdirp');
// const fs = require('fs');
// const fuzzyPath = require('inquirer-fuzzy-path');
const path = require('path');
inquirer.registerPrompt('fuzzypath', require('inquirer-fuzzy-path'));

const projectPath = path.resolve(__dirname, "../../");
const appPath = path.resolve(__dirname, "../../app/");

// console.log(projectPath);

let setSaveDirectory = () => {

	let questions = [
		{
			type: 'fuzzypath',
			name: 'path',
			excludePath: nodePath => nodePath.startsWith('node_modules'),
			itemType: 'directory',
			rootPath: appPath,
			// rootPath: 'app',
			message: 'Select a target directory for your animate JS files:',
			default: '../../app/js/',
			// default: 'js/',
			suggestOnly: false
		}
	];


	return inquirer.prompt(questions).then(function (answers) {
		console.log(answers);
	});
};


return setSaveDirectory();

// mkdirp('../../app/js/svg', (err) => {
// 	if (err) {
// 		throw err;
// 	} else {
// 		copydir('assets', '../../app/js/svg', function (err) {
// 			if (err) {
// 				throw err;
// 			}
// 		});
// 	}
// });
