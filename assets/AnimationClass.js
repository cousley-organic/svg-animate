let createjs = createjs || {};
let AdobeAn = AdobeAn || {};

// let handleComplete = Symbol(),
// 	let startAnimation = Symbol('startAnimation'),
// 	anim_container = Symbol('anim_container'),
// 	canvas = Symbol('canvas'),
// 	libBuilder = Symbol('libBuilder'),
// 	comp = Symbol('comp'),
// 	dataAnimateClass = Symbol('dataAnimateClass');
//
const getAnimationClass = (id) => `${id.charAt(0).toUpperCase() + id.slice(1)}Animation`;

class Animation {
	constructor(id) {
		this.id = id;
		this.anim_container = document.querySelector(`[data-animate-id=${id}]`);
		this.canvas = this.anim_container.getElementsByTagName('canvas')[0];
		this.libBuilder = window[`${id}Lib`];
		return this;
	}
	//This function is always called, irrespective of the content.
	//You can use the variable "stage" after it is created in token create_stage.
	handleComplete(evt, comp) {
		this.lib = comp.getLibrary();
		let ss = comp.getSpriteSheet();
		this.lib.canvas = this.canvas;
		let exportRoot = new this.lib.script();
		this.stage = new this.lib.Stage(this.canvas);
		this.stage.addChild(exportRoot);
		AdobeAn.compositionLoaded(this.lib.properties.id);

		// if visible on load ?  run animation : hide animation
		(this.display === "block") ? this.startAnimation(this.lib, this.stage) : this.hide();
		return this;
	};
	show(cb) {
		this.canvas.style.display = "block";
		this.display = "block";
		if (cb) cb();
		return this;
	}
	hide(cb) {
		this.canvas.style.display = "hidden";
		this.display = "none";
		if (cb) cb();
		return this;
	}
	//Registers the "tick" event listener.
	startAnimation(lib, stage) {
		this.show(function () {
			createjs.Ticker.setFPS(lib.properties.fps);
			createjs.Ticker.addEventListener("tick", stage);
		});
		return this;
	};
	reset() {
		this.hide(this.stage.getChildAt(0).gotoAndStop(0));
		return this;
	};
	init(timeOut, hiddenOnLoad) {
		this.display = (hiddenOnLoad || false) ? "none" : "block";
		this.timeOut = timeOut || 0;
		this.libBuilder(createjs, AdobeAn, this.canvas);
		let compID = Object.keys(AdobeAn.compositions)[0];
		this.comp = AdobeAn.getComposition(compID);
		this.handleComplete({}, this.comp);
		return this;
	};
	run() {
		this.show(function(){
			setTimeout(this.handleComplete.bind(this, {}, this.comp), this.timeOut)
		}.bind(this));
		return this;
	}
}
