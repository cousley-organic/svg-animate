/*************************
*
*	 ███████╗██╗  ██╗ █████╗ ███╗   ███╗██████╗ ██╗     ███████╗
*	 ██╔════╝╚██╗██╔╝██╔══██╗████╗ ████║██╔══██╗██║     ██╔════╝
*	 █████╗   ╚███╔╝ ███████║██╔████╔██║██████╔╝██║     █████╗
*	 ██╔══╝   ██╔██╗ ██╔══██║██║╚██╔╝██║██╔═══╝ ██║     ██╔══╝
*	 ███████╗██╔╝ ██╗██║  ██║██║ ╚═╝ ██║██║     ███████╗███████╗
*	 ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝╚═╝     ╚══════╝╚══════╝
*
* 	The animation ID is 'bar'
*	timeout is 0 seconds
* 	hide on load = true
*
*	1) Init the animation class passing one param: (id)
*		id = string, no default, name of the animation
*
*	const BarAnimation = new Animation('bar')
*
*
*	2) load it passing two params: (timeout, isHidden)
* 		timeout = integer, default 0, number of milliseconds you want the animation to delay after you run it
*		isHidden = boolean, default = false, should the animation be hidden on load
*
*		You can init it immediately:
*			const BarAnimation = new Animation('bar').init(0, true);
*
*		...or later:
*			BarAnimation.init(0, true);
*
*
*
*	3) run it using .run()
*		BarAnimation.run();
*
 *************************/