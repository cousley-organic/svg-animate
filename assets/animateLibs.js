/*************************
 *
 *	 ███████╗██╗  ██╗ █████╗ ███╗   ███╗██████╗ ██╗     ███████╗
 *	 ██╔════╝╚██╗██╔╝██╔══██╗████╗ ████║██╔══██╗██║     ██╔════╝
 *	 █████╗   ╚███╔╝ ███████║██╔████╔██║██████╔╝██║     █████╗
 *	 ██╔══╝   ██╔██╗ ██╔══██║██║╚██╔╝██║██╔═══╝ ██║     ██╔══╝
 *	 ███████╗██╔╝ ██╗██║  ██║██║ ╚═╝ ██║██║     ███████╗███████╗
 *	 ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝╚═╝     ╚══════╝╚══════╝
 *
 * 	The animation ID is 'bar'
 *
 * 	1) Open the exported JS file and copy the code from first parentheses
 *		function (cjs, an) {};
 *
 *	2) Copy it here, naming it after the selected id
 *		let barLib = function (cjs, an) {};
 *
 *	3) add canvas as an argument
 *	let barLib = function (cjs, an, canvas) {};
 *
 *************************/