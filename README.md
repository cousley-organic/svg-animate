#svg-animate
 
Easy library for creating SVG animations made with Adobe Animate


![npm](https://img.shields.io/badge/npm-3.10.9-orange.svg)

***
## Table of Contents
- [About](#markdown-header-about)
- [Install](#markdown-header-install)
- [Setup](#markdown-header-setup)
    - [I. Include the Adobe Library](#markdown-header-i-include-the-adobe-library)
    - [II. Create and include `animate.js`](#markdown-header-ii-create-and-include-animatejs)
    - [III. Move the animation libraries into `animate.js`](#markdown-header-iii-move-the-animation-libraries-into-animatejs)
- [Playing the animation](#markdown-header-playing-the-animation)
    - [I. Pre-loading the animation](#markdown-header-i-pre-loading-the-animation)
    - [II. Running the animation](#markdown-header-ii-running-the-animation)
- [Update](#markdown-header-update)
- [Features](#markdown-header-features)
- [Contribute](#contribute)

## About
> A modular way to animate SVGs created in Adobe Animate using Object Oriented Programming.
>
> Easy to update when Adobe makes revisions on their code

## Install

Install with [npm](https://www.npmjs.com/):

```bash
$ npm i ssh://bitbucket.org/cousley-organic/svg-animate.git --save
```

Include the module's `index.js` in your gulpfile's script-building task

    :::javascript
    gulp.task('scripts', ['lint'], function () {
       return gulp.src([
           ...

           './node_modules/svg-animate/index.js', // svg-animate's index.js

           ...
       ])
       ...
    });

## Setup

### I. Include the Adobe Library
You will need to include this JS library on your page to help with all the animation functions (you'll see this inclusion in the exported HTML file):
```
<script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
```

### II. Create and include `animate.js`
You will want to combine portions of each exported JS file(s) into a single JS file. This will make it easier to update in the future if needed.

1. Create an `animate.js` file somewhere inside your ```./app/js``` directory.

2. Include it inside your gulpfile's script task, below the svg-animate's ```index.js```.

        :::javascript
        gulp.task('scripts', ['lint'], function () {
           return gulp.src([
               ...

               './node_modules/svg-animate/index.js', // svg-animate's index.js
               './app/js/animate.js', // animate.js

               ...
           ])
           ...
        });

### III. Move the animation libraries into `animate.js`
1. Open each exported JS file and collapse the code.

        :::javascript
        /* Inside exported JS file for 'handshake' animation */

        (function (cjs, an) {...})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
        var createjs, AdobeAn;


2. Highlight the portion below.

        :::javascript
        (

            function (cjs, an) {...}
            ⭶          ⭱          ⭷
                 THIS PART HERE

        )(createjs = createjs||{}, AdobeAn = AdobeAn||{});

3. Copy and paste it into the new `animate.js`. Then, think of an `id` that uniquely identifies the animation.
   
    1. An animation of people shaking hands could have a `id` of `handshake` 
      
    2. Assign the function to the variable `{id}Lib`. Don't include the brackets; they just signify here that `id` is a variable.
    
    3. Add the param `canvas`

            :::javascript
            /*
            *  Inside animate.js
            *
            *  id='handshake'
            */

            const handshakeLib = function (cjs, an, canvas) {...};          🡸 Add and rename it here
                  ⭱⭱⭱⭱⭱⭱⭱⭱⭱                         ⭱⭱⭱⭱⭱⭱
               ID='handshake'                     ADD CANVAS

4. Inside each script, make note of the content stager method. In this case, it's `handshakeicon_desktop_011`

        :::javascript
        // stage content:
        (lib.handshakeicon_desktop_011 = function(mode,startPosition,loop)...

5. Rename them all to `script`:
    
        :::javascript
        // stage content:
        (lib.script = function(mode,startPosition,loop)...

6. Add the the exported HTML file content where you wish to place the SVGs. Be sure to add `data-animate-class="{id}"` on the div that contains the canvas element

        :::html
        <div data-animate-class="{id}">
           <canvas class="img" width="188" height="88" style="display: block; background-color:rgba(252, 198, 10, 1.00);"></canvas>
        </div>

   

## Playing the animation
### I. Pre-loading the animation
To pre-load the animations into JS, use the following formula:

1. For each animation, instantiate a new `Animation` class and pass in the `id`. Declare the class as `{id}Animation`, again omitting the brackets.
    
    **IMPORTANT:** Make sure `{id}` is **Capitalized**.
    Classes ***always*** begin with a **Capital** letter.
    
        :::javascript
        // id = 'handshake'
        const HandshakeAnimation =
           new Animation('handshake');      🡸 Instantiate it
                          ⭱⭱⭱⭱⭱⭱⭱⭱⭱
                         PASS IN ID
                         
2. Chain `.init()` to your new instance, passing in the number of milliseconds you want the animation to delay for when you run it. If no delay is desired, pass nothing.

        :::javascript
        // id = 'handshake'
        const HandshakeAnimation =
           new Animation('handshake')
               .init(3500);             🡸 Loads it with a delay of 3.5 seconds
                     ⭱⭱⭱⭱
                  DELAY IN MS

        // or

        const HandshakeAnimation = new Animation('handshake');

        HandshakeAnimation.init(3500);  🡸 Loads it with a delay of 3.5 seconds

        // also

        const HandshakeAnimation = new Animation('handshake');

        HandshakeAnimation.init();      🡸 Loads it with no delay
        

### II. Running the animation
3. To run the animations chain `.run()` to your new instance.

        :::javascript
        // id = 'handshake'
        const HandshakeAnimation =
                new Animation('handshake').init(3500).run();

        // or

        const HandshakeAnimation = new Animation('handshake').init()

        HandshakeAnimation.run();

## Update

### Update `svg-animate`

```bash
npm outdated
```
```bash
npm update svg-animate --save
```

### Update animation libraries in `animate.js`
Just replace the old {id}Lib with the old. Just remember to rename

